const path = require('path');

const BASE_PATH = path.join(__dirname,'..','client','routes','v1')


const ALL_ROUTES = (app)=>{
    app.use('/tmx/v1',
        require(path.join(BASE_PATH,'auth')),
        require(path.join(BASE_PATH,'profile_pic')),
        require(path.join(BASE_PATH,'talentProfile')),
        require(path.join(BASE_PATH,'subscription')),
        require(path.join(BASE_PATH,'landingPage')),
    )

}

module.exports = ALL_ROUTES