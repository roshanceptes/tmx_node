const express = require("express")
require('dotenv').config();

const bodyParser = require('body-parser');

const cors = require('cors')

const jsForceConnection = require('./utilities/jsForceConnection')

const app = express()

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());
// const corsConn = require('./utilities/cors')
// app.use(corsConn)

app.use(express.static('assets/'));

app.use(cors())
//app.use(cors)
require('./utilities/all_routes')(app) //All Routes handle here

/************************************* Express middleware or Special function to handle errors ************************** */

app.use((req, res, next) => {
    console.log(req.headers.host);
    res.status(405).json({ error: `${req.method} Method is not supported` });
  });
  
  app.use((error, req, res, next) => {
    const statusCode = error.statusCode || res.statusCode || 500;
    const errorMessage = error.message || error;
  
    if (statusCode === 500) console.log("app.js", error);
    else console.log("app.js user error", error);
  
    res.status(statusCode).json({ message: errorMessage });
  }); //End of error handling middleware
  
  /************************************ End Express middleware or Special function to handle errors ************************ */


  /************************************PayLoad*************************** */
  app.use((req,res,next)=>{
    req.payload ={...req.body,...req.query,...req.params};
    next();
  })

  /**********************************End of the payload */
const PORT =process.env.PORT;
app.listen(PORT,()=>{
    console.log(`App running in port ${PORT}`)
})