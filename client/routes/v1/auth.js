const express = require('express')

const router = express.Router();
const {body} = require('express-validator')

/*****************************************************Middleware******************************** */

const I_U_E_M = require('../../middleware/v1/is_user_exists_middleware')

/**************************************************CONTROLLER**************************************** */

const Signup_Controller =require('../../controller/v1/auth/signUp_Controller')

const Login_Controller = require('../../controller/v1/auth/login_Controller');

const Send_OTP_COntroller = require('../../controller/v1/auth/sendOTP_Controller');

const OTP_Verify_Controller = require('../../controller/v1/auth/verify_OTP_Controller');

const forgot_Password_Controller = require('../../controller/v1/auth/forgot_password_Controller');

const Resend_OTP = require('../../controller/v1/auth/resendOTP_controller');

/*************************************************ROUTES*************************************** */

router.post('/signup',I_U_E_M,Signup_Controller);

router.post('/login',I_U_E_M,Login_Controller);

router.post('/sendOTP',I_U_E_M,Send_OTP_COntroller);

router.post('/verifyOTP',OTP_Verify_Controller);

router.post('/forgetPassword',forgot_Password_Controller);

router.post('/resendOTP',Resend_OTP);

module.exports = router