const express = require('express');

const router = express.Router()

const multer = require('multer')

const path = require('path')
const storage = multer.diskStorage({
  
    destination:function(req,file,cb){
        let dest=path.join(__dirname,'..','..','..','assets','profile_pic')
       // console.log(dest)
        cb(null,dest)
    },
    filename:function(req,file,cb){
       // console.log(file)
        cb(null, file.originalname)
    }
})

const upload = multer({storage:storage})

/***********************************************************CONTROLLER******************** */

const ProfilePic_controller = require('../../controller/v1/upload_profile_controlle')

/*****************************************************End of the Controller******************* */

/********************************************MIDDLEWARE************************************* */
const Token_Verify_Middleware = require('../../middleware/v1/token_verify_middleware')
/****************************************End of the  Controller************************* */

/******************************************************ROUTES************************** */

router.post('/talentProvider/profilePic',upload.single('profilePic'),Token_Verify_Middleware,ProfilePic_controller)

module.exports = router