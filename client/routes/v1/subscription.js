const express = require('express');

const router = express.Router();
/***********************************************Controller******************************* */
const Subscription_Controller = require('../../controller/v1/subscription_controlle')
/**********************************************Middleware***************************** */
const Token_Verify_Middleware = require('../../middleware/v1/token_verify_middleware')

const Is_User_Exits_Middleware = require('../../middleware/v1/is_user_exists_middleware')

/********************************************Routes************************************ */
router.post('/subscription',Token_Verify_Middleware,Is_User_Exits_Middleware,Subscription_Controller)

module.exports = router