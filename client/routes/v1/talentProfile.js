const express = require('express')

const router = express.Router();
/**************************************Controller******************************* */
const talent_profile = require('../../controller/v1/talentProfile');

const Create_Talent_Card_Controller = require('../../controller/v1/talentCard_Controller');

/*****************************************MIDDLEWARE******************************/
const Token_Verify_Middleware = require('../../middleware/v1/token_verify_middleware');
const I_U_E_M = require('../../middleware/v1/is_user_exists_middleware')
/*********************************************routes *******************************/
router.get('/user',Token_Verify_Middleware,talent_profile.Get_talent_profile);

router.post('/talentProfile',Token_Verify_Middleware,I_U_E_M,Create_Talent_Card_Controller.Create_Talent_Card_Controller)

module.exports = router