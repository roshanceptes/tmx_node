const jwt = require('jsonwebtoken');

const Token_Verify_Middleware = (req, res, next) => {

    const inputToken = req.header('Authorization');
   // console.log(inputToken)

    if (!inputToken) {
        return res.status(422).json({ Error: "Token is not available" })
    }
    else if (inputToken) {
        const token = inputToken.split(' ')[1];
        //console.log(token)
        jwt.verify(token, process.env.JWT_SECRET, (error, result) => {
            if (error) {
               return res.status(401).json({ Error: "Unauthorized" })
            }
           // console.log(result)
            req.userID=result.userId,
           // console.log(req.userID)
            req.email = result.email
            console.log(req.email)

            next();

        }) // End of the JWT

    } // End of the else if block

}// end of the function

module.exports = Token_Verify_Middleware