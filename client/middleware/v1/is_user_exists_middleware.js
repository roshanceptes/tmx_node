const conn = require('../../../utilities/jsForceConnection')


const Is_User_Exists_Middleware =async(req,res,next)=>{
    const email = req.email || req.body.email

    try{
        await conn.sobject('Account').find({Email_ID__c:email}).execute((err,records)=>{
           // console.log(records.length)
            if(records.length==0){
                req.is_user_exists = false;
                return next();
            }
            req.is_user_exists =true
            req.userId = records[0].Id
            // console.log(req.userId)
            // console.log(records[0].Id)
            return next();
        })

    }
    catch(err){
        console.log(err)
    }
}

module.exports=Is_User_Exists_Middleware