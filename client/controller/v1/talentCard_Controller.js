const conn = require('../../../utilities/jsForceConnection');

exports.Create_Talent_Card_Controller = async (req, res, next) => {

    try {
        const { availability, talent_provide, designation, year, months, workType, workLocation,
            provide_talent_for, provide_talent_in, skills, proficient, populate_talent_from,
            whom_talent_seeker, whom_designation, whom_location, whom_from, whom_to, whom_annual_earning, yes_or_no,
            linkedIn_url, resume_url
        } = req.body
        let whereClause;
        if (populate_talent_from == "Don't have any(Fill it manually)") {
            whereClause = {
                Whom_Talent_Seeker__c: whom_talent_seeker,
                Whom_Designation__c: whom_designation,
                Whom_Location__c: whom_location,
                Whom_From__c: whom_from,
                Whom_To__c: whom_to,
                Whom_Annual_Earning__c: whom_annual_earning,
                Whom_Currently_Providing_Talent__c: yes_or_no
            }
        }
        else if (populate_talent_from == "LinkedIn") {
            whereClause = {
                LinkedIn_URL__c: linkedIn_url,
            }
        }
        else {
            whereClause = {
                Resume_URL__c: resume_url
            }
        }


        conn.sobject('Account').find({Id: req.userId }).execute((err, records) => {
            console.log(req.userId)
            if (records == false) {
                return res.json({ message: "User not Available " })
            } //records checking 

            console.log(records[0].Id)
            conn.sobject('Account').update({
                Id: records[0].Id,
                //who
                Availability__c: availability, //availability
                Current_Talent_Provider__c: talent_provide, //
                Current_Designation__c: designation,//designation
                Total_Experience_yrs__c: year,
                Total_Experience_months__c: months,

                //where
                Work_Type__c: workType,//work loacation,
                Work_Location__c: workLocation,
                //whom
                Service_To_Talent_Seekers__c: provide_talent_for,
                Job_Function__c: provide_talent_in, //provide talent in
                Skill_Set__c: skills,
                Proficient__c: proficient,

                //whom
                ...whereClause
            })
                conn.sobject('Profile__c').create({
                    Name: req.body.profileName,//profile name
                    Account__c: records[0].Id, //name of the account 
                   // RecordTypeId: req.body.RecordType,// like academics,
                    Institute__c: req.body.institute,
                    Qualification__c: req.body.qualification,
                    Specialization__c: req.body.specialization,
                    Year_Of_Entry__c: req.body.year_of_entry,
                    Year_of_Pass__c: req.body.year_of_pass,
                    Skill_Set__c: skills
                })
                conn.sobject('Account').find({ Name: provide_talent_for }).execute((err, rets) => {
                    let Intersted_Company_obj
                    if (rets.length === 0) {
                        conn.sobject('Interested_Company__c').create({
                            Talent_Provider__c: records[0].Id,
                            Company_Interested__c: provide_talent_for
    
                        })
                    }
                    else {
                        conn.sobject('Interested_Company__c').create({
                            Talent_Provider__c: records[0].Id,
                            Talent_Seeker__c: rets[0].Id
    
                        })
                        Intersted_Company_obj = {
                            Talent_Seeker__c: provide_talent_for
                        }
                    }
                }) //searching for talent seeker 

                    .then(result => {
                        res.json({
                            message: "Talent card created "
                            // message:"Talent card created " + result.id
                        })
                    })
                    .catch(err => next(err))
        })
    }

    catch (err) { next(err) }

}


