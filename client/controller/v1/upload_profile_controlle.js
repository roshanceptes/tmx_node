const conn = require('../../../utilities/jsForceConnection')
const path = require('path');

const fs = require('fs')
  //Reaminig beacuse the profile pic field is not there in Account
const ProfilePic_controller = (req,res,next)=>{

    try{

        conn.sobject('Account').find({Email_ID__c:req.email}).execute((err,records)=>{
            if(records.length==0){
                res.status(204).json({Error:"Something went wrong please try again"})
            }

            
            conn.sobject('Account').update({
                Id:req.userID,
                profilePic__c:`${req.file.fieldname}/${req.file.filename}`
            })
            .then(result=>{ 
                if (!result.success) {
                    return res.json({ message: "Something went wrong, please try again" })
                }
                res.status(200).json({ message: "profile pic uploaded", id: result.id });
            })
            .catch(err=>next(err))
        })

    }

    catch(err){next(err)}
}

module.exports = ProfilePic_controller
