const bcrypt = require('bcrypt')

const conn = require('../../../../utilities/jsForceConnection');
const OTP_Gen = require('../../../helpers/generate_otp')();

const SignUp_Controlle = async (req, res, next) => {
    if (req.is_user_exists) {
        return res.status(409).json({ message: "User Already Exits" });
    }
    try {
        
        const { fName, lName, email, password, phone } = req.body
        const hashedPassword = await bcrypt.hash(password, 12);
       await conn.sobject('Account').create({
        Name: fName,Last_Name_del__c: lName,Email_ID__c: email, Phone: phone,
        Password__c: hashedPassword,
        User_Name__c:email,
        Full_Name__c :`${fName} ${lName}`
        })
            .then(async result => {
                if (!result.success) {
                    return res.json({ message: "Something went wrong, please try again" })
                }
               await conn.sobject('Account').update({
                    Id:result.id,
                    OTP__C: OTP_Gen.Otp,
                	Expire_Time__c: OTP_Gen.expiryDatetime
                },function(err,rets){
                    if (err || !rets.success) { return console.error(err, rets); }
                    res.status(200).json({ message: "User created successfully", id: result.id });
                })

                // res.status(200).json({ message: "User created successfully", id: result.id });
            })
            .catch(err => { next(err) })
    }

    catch (err) { next(err) }

}

module.exports = SignUp_Controlle