const conn = require('../../../../utilities/jsForceConnection')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Login_Controller = async (req, res, next) => {
    if (!req.is_user_exists) {
        return res.status(404).json({ error: "User not Found" });
    }
    try {
        await conn.sobject('Account').find({ Email_ID__c: req.body.email }).execute((err, records) => {
            //  console.log(records)
            if (records == false) {
                return res.json({ error: "User not Available " })
            }
            else if(records[0].Authenticated_Successful__c==false){
                return res.json({error:"Please verify the email for two step verification"})
            }
            else {
                bcrypt.compare(req.body.password, records[0].Password__c).then((result) => {
                    //   console.log(result);
                    if (result === true) {
                        const token = jwt.sign({
                            email: records[0].Email_ID__c,
                            userId: records[0].Id.toString(),
                        }, process.env.JWT_SECRET, { expiresIn: '1h' });
                        res.status(200).json({ 
                          
                            message: "User available",
                            token: token, 
                            name: records[0].Name, 
                            userId: records[0].Id.toString(),
                            payload:{...req.body,...req.query,...req.params}
                       
                         });
                    }
                    else {
                        res.json({ error: "Invalid password" });
                    }
                })
            }
        })
    }
    catch (err) { next(err) }
}

module.exports = Login_Controller