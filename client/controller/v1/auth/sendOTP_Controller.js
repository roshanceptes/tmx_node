const conn = require('../../../../utilities/jsForceConnection')

const OTP_Gen = require('../../../helpers/generate_otp')();
const  Send_OTP_Controller =async(req,res,next)=>{
    if (!req.is_user_exists) {
        return res.status(404).json({ message: "User not there in this emailId" });
    }
    try{
        console.log(req.userId)
        //console.log(OTP_Gen.Otp)
        conn.sobject('Account').update({
            Id:req.userId,
            OTP__C :OTP_Gen.Otp,
            Expire_Time__c:OTP_Gen.expiryDatetime
        },function(err,rets){
            if (err || !rets.success) { return console.error(err, rets); }
            res.json({
                message:`Otp send to your email id ${req.body.email}`,
                userId:req.userId
            })
            // console.log('Updated Successfully : ' + ret.id);
        })
        
    }
    catch(err){next(err)} 
    
}

module.exports = Send_OTP_Controller