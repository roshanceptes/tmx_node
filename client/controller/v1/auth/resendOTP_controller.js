const conn = require('../../../../utilities/jsForceConnection');

const OTP_Gen = require('../../../helpers/generate_otp')();
const Resend_OTP = (req,res,next)=>{

    try{
        const {userId} =req.query

        conn.sobject('Account').find({Id:userId}).execute((err,records)=>{
            if(records.length ==0){
                res.status(401).json({message:""})
            }
            conn.sobject('Account').update({
                Id:userId,
                OTP__C :OTP_Gen.Otp,
                Expire_Time__c:OTP_Gen.expiryDatetime
            },function(err,rets){
                if (err || !rets.success) { return console.error(err, rets); }
                res.json({
                    message:"Otp send to your email id",
                    userId:userId,
                   // OTP:rets.OTP__C
                })
                // console.log('Updated Successfully : ' + ret.id);
            })
        })

        

    }

    catch(err){next(err)}

}

module.exports = Resend_OTP