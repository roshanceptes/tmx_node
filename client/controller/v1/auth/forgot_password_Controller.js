const conn = require('../../../../utilities/jsForceConnection');
const bcrypt = require('bcrypt');


const forgot_Password_Controller = async(req,res,next)=>{

    try{
        const newPassword = await bcrypt.hash(req.body.newPassword ,12)
        console.log(newPassword)
       // console.log(req.Id)

       console.log(req.query.Id)

    conn.sobject('Account').find({Id:req.query.Id}).execute((err,records)=>{
        if(records.length ==0){
            res.status(401).json({message:"Something went wrong,Please try again"})
        }
        
       // console.log(bcrypt.hash(req.query.newPassword,12))
        conn.sobject('Account').update({
            Id:req.query.Id,
            Password__c:newPassword
        },function(err,rets){
            if (err || !rets.success) { return console.error(err, rets); }
            res.json({
                message:`Password changed `
            })
        })
    })

    }

    catch(err){next(err)}
}

module.exports = forgot_Password_Controller