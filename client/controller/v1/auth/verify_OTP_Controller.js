const conn = require('../../../../utilities/jsForceConnection');

 const Verify_OTP_Controller = (req,res,next)=>{

    try{

        conn.sobject('Account').find({OTP__c:req.body.OTP}).execute((err,records)=>{
            if(!req.body.OTP){
                return res.json({message:"please enter the otp"})
            }
           else if(records.length ==0){
                res.status(401).json({message:"OTP not verified. Please try again"})
            }

            conn.sobject('Account').update({
                Id:records[0].Id,
                Authenticated_Successful__c:true
            })
            req.Id =records[0].Id,
            req.email = records[0].Email_ID__c,
            console.log(req.email)
            console.log(req.Id)
            res.status(201).json({
                message:"OTP Verified",
                Id:records[0].Id
            })
            // next(); 
            
        })

    }

    catch(err){next(err)}

 }

 module.exports = Verify_OTP_Controller