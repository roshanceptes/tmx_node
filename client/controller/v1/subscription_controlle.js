const conn = require('../../../utilities/jsForceConnection')


const Subscription_Controller = (req,res,next)=>{
   // console.log("Id will be" + req.userID)

    try{
        const {talentIn,cost,startTime,endTime,discountCriteria} = req.body
        let whereClause ;
        if(talentIn=='Full time employment'){
            whereClause ={
                Full_time_employment__c:true,
                Full_time_employment_Cost__c:cost
            }
        }
        else if(talentIn=='Fixed term engagement'){
            whereClause={
                Fixed_term_engagement__c:true,
                Fixed_term_engagement_Cost__c:cost
            }
        }
        else {
            whereClause={
                Flexed_tuned_Experienced_mode__c:true,
                Flexed_tuned_Experienced_mode_Hourly_Rat__c:cost
            }
        }
        conn.sobject('Account').find({Id: req.userId}).execute((err,records)=>{
            if(err){
                res.json({message:"Something went wrong Please try"})
            }
           // console.log(records.length)
            // if (records.length >0) {
            //    // console.log("logged")
            //    // console.log(req.userID)
            //     return res.json({ message: "User Already Subscribed" })
            // }
          //  console.log("false")
            conn.sobject('Account').update({
                Id:records[0].Id,
                ...whereClause,
                Flex_Start_Time__c:startTime,
                Flex_End_Time__c:endTime,
                Discount_Criteria__c:discountCriteria,
         
              //upload proof remaining
    
            })

            conn.sobject('Subscription__c').create({
                Account__c:records[0].Id,
                Account_Type__c:req.body.accountType,
               Subscription_Amount__c:req.body.subscriptionAmount
            })
            
            .then(result => {
                if (!result.success) {
                    return res.json({ message: "Something went wrong, please try again" })
                }
                res.status(200).json({ message: "Subscription added", id: result.id });
            })
            .catch(err => { next(err) })

        })
       

    }

    catch(err){next(err)}

}

module.exports = Subscription_Controller